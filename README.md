# Aide et ressources de UFO pour Synchrotron SOLEIL

## Résumé

- Pour la tomographie : Pre-traitement, reconstruction et post-traitement
- Open source

## Sources

- Code source: https://salsa.debian.org/science-team/ufo-core - https://salsa.debian.org/science-team/ufo-filters
- Documentation officielle: https://ufo-core.readthedocs.io/en/latest/

## Navigation rapide

| Tutoriaux |
| - |
| [Tutoriel d'installation officiel](https://ufo-core.readthedocs.io/en/latest/install/index.html) |
| [Tutoriaux officiels](https://ufo-core.readthedocs.io/en/latest/using/index.html) |

## Installation

- Systèmes d'exploitation supportés: Linux, MacOS, Necessite une pile OpenCL sur la machine
- Installation: Facile (tout se passe bien), Paquet debian existant, pour MacOS compilation (avec complication pour la partie OpenCL)

## Format de données

- en entrée: raw (binaire sans entête), hdf5, tiff (et autres formats images /classiques/)
- en sortie: raw, hdf5, pile tiff
- sur un disque dur
